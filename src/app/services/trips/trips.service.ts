import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { Trip } from 'src/app/entities/trip';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class TripsService {

  constructor(private _storage: Storage, private _userService: UserService) { }

  //Retourne le voyage adéquat à l'ID fournit en paramètre
  getTripById(id: number): Trip{
    return this._userService.currentUser.trips.find(trip => trip.id == id);
  }
}
