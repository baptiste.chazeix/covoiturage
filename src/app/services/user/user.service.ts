import { Injectable } from '@angular/core';
import { User } from '../../entities/user';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  currentUser: User

  constructor(private _storage: Storage) { }

  //Définit pour l'application l'utilisateur courant
  set(user: User){
    this.currentUser = user;
  }

  //Sauvegarde les données de l'utilisateur courant
  save(){
    this._storage.set(this.currentUser.name, this.currentUser);
  }
}
