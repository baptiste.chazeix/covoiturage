import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import {formatDate} from '@angular/common';
import { Trip } from '../../entities/trip';
import { UserService } from '../../services/user/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-trips',
  templateUrl: 'my-trips.page.html',
  styleUrls: ['my-trips.page.scss'],
})
export class MyTripsPage implements OnInit {

  trips: Trip[] = []

  constructor(private _alertController: AlertController, private _userService: UserService, private _router: Router, private _route: ActivatedRoute) {

  }

  ngOnInit() {
    this.trips = this._userService.currentUser.trips;
  }

  //Ouvre une alerte qui permet de créer un voyage
  async addTrip() {
    const alert = await this._alertController.create({
      cssClass: 'my-alert',
      header: 'Trip',
      inputs: [
        {
          name: 'from',
          type: 'text',
          placeholder: 'From'
        },
        {
          name: 'to',
          type: 'text',
          placeholder: 'To'
        },
        {
          name: 'kilometers',
          type: 'number',
          placeholder: 'Kilometers',
          min: 0
        },
        {
          name: 'seat',
          type: 'number',
          placeholder: 'Seat',
          min: 0
        },
        {
          name: 'date',
          type: 'date',
          min: formatDate(new Date(), 'yyyy-MM-dd', 'en')
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {            
            let trip = {
              id: this.makeID(),
              from: data.from,
              to: data.to,
              kilometers: data.kilometers,
              seat: data.seat,
              seatAvailable: data.seat,
              date: data.date
            }
            /**
             * Si l'utilisateur courant n'a pas de voyage, initialise sa liste de voyage.
             * Puis ajoute le nouveau voyage dans sa liste.
             * Et sauvegarde.
             */ 
            if(!this._userService.currentUser.trips){
              this._userService.currentUser.trips = []
            }
            this._userService.currentUser.trips.push(trip);
            this._userService.save();
            
          }
        }
      ]
    });

    await alert.present();
  }

  //Retourne un ID unique pour un voyage
  makeID(): number{
    if (this.trips.length != 0) {
        return Math.max(...this.trips.map(t => t.id))+1;
    }
    return 0;
  }

  //Redirige l'utilisateur vers la page de détail du voyage
  showDetail(trip: Trip){
    this._router.navigate([trip.id.toString()], { relativeTo: this._route });
  }

  //Supprime le voyage en paramètre
  delete(trip: Trip){
    let index = this.trips.indexOf(trip);
    this.trips.splice(index,1);
    this._userService.save();
  }

}
