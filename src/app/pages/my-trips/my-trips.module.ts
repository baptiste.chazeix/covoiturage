import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { MyTripsPage } from './my-trips.page';

import { MyTripsPageRoutingModule } from './my-trips-routing.module';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyTripsPageRoutingModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyTripsPage
      }
    ])
  ],
  declarations: [MyTripsPage]
})
export class MyTripsPageModule {}
