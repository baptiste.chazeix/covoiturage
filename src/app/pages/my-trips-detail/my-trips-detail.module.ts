import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyTripsDetailPageRoutingModule } from './my-trips-detail-routing.module';

import { MyTripsDetailPage } from './my-trips-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyTripsDetailPageRoutingModule
  ],
  declarations: [MyTripsDetailPage]
})
export class MyTripsDetailPageModule {}
