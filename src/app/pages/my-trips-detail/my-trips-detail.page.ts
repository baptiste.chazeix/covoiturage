import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { Trip } from 'src/app/entities/trip';
import { TripsService } from 'src/app/services/trips/trips.service';

@Component({
  selector: 'app-my-trips-detail',
  templateUrl: './my-trips-detail.page.html',
  styleUrls: ['./my-trips-detail.page.scss'],
})
export class MyTripsDetailPage implements OnInit {

  trip: Trip;

  constructor(private _route: ActivatedRoute, private _userService: UserService, private _tripsService: TripsService) { }

  //Récupère le voyage via l'ID en paramètre de l'URL
  ngOnInit() {
    let id = Number(this._route.snapshot.paramMap.get('id'));
    this.trip = this._tripsService.getTripById(id);    
  }

}
