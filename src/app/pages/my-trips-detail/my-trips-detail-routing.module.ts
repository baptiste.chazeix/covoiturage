import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyTripsDetailPage } from './my-trips-detail.page';

const routes: Routes = [
  {
    path: '',
    component: MyTripsDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyTripsDetailPageRoutingModule {}
