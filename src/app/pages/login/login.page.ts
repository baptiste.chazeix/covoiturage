import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Storage } from '@ionic/storage';
import { UserService } from '../../services/user/user.service';
import { User } from '../../entities/user';
import { Trip } from '../../entities/trip';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string = ""

  constructor(private _router: Router, private _storage: Storage, private _userService: UserService) { }

  ngOnInit() {
  }

  /**
  ** Permet de stocker l'état de connexion + Redirection vers la page d'accueil
  **/
  login() {
    if(this.username){
      this._storage.get(this.username).then((user: User) => {
        if(user){
          // user.trips = [];
          // this._storage.set(this.username, user);
          this._userService.set(user);
        } else {
          let newUser: User = { name: this.username, trips: [] };
          this._storage.set(this.username, newUser);
          this._userService.set(newUser);
        }
        
        // Sauvegarde de l'état de connexion
        this._storage.set('userAuthenticated', true);

        // Redirection vers la page d'accueil
        this._router.navigate(['/my-trips']);
      });
    }
  }
}
