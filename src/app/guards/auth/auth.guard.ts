import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private _router: Router, private _storage: Storage){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      // On récupère la valeur de userAuthenticated en BDD
    return this._storage.get('userAuthenticated').then((userAuthenticated) => {
      if (userAuthenticated) {
        // Déjà connecté : on redirige l'utilisateur vers la page d'accueil
        return true;
      } else {
        // return false;
        // Non connecté : on redirige l'utilisateur vers la page de Login
        this._router.navigate(['/login']);
      }
    });
  }
  
}
