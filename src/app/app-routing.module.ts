import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'my-trips',
    loadChildren: () => import('./pages/my-trips/my-trips.module').then( m => m.MyTripsPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'my-trips/:id',
    loadChildren: () => import('./pages/my-trips-detail/my-trips-detail.module').then( m => m.MyTripsDetailPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'my-trips',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'my-trips'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
