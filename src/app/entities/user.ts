import { Trip } from './trip';

export interface User {
    name?: string
    firstname?: string
    email?: string
    car?: string
    trips?: Trip[]
}