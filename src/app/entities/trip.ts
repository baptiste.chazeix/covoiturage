import { User } from './user'
import { Result } from './result'

export interface Trip {
    id: number
    from?: string
    to?: string
    kilometers?: number
    seat: number
    seatAvailable: number
    passengers?: [User]
    date: string
}